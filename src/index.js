// TODO: resize not working in portfolio (narrow to wide)
// TODO: implement JSDoc
// TODO: dynamic add / remove slides
// TODO: figure out gitlab pages for hosting docs and demo

import './index.scss';

// TODO: need a way to set currentItem from constructor
// TODO: resizing while in motion causes issues, inspect
export default class Carousel {
  constructor(el, options) {
    console.log('DS Carousel v0.2.4');
    this.addSlide = this.addSlide.bind(this);
    this.removeSlide = this.removeSlide.bind(this);
    this.pointerdown = this.pointerdown.bind(this);
    this.touchstart = this.touchstart.bind(this);
    this.pointerenter = this.pointerenter.bind(this);
    this.pointermove = this.pointermove.bind(this);
    this.touchmove = this.touchmove.bind(this);
    this.wheel = this.wheel.bind(this);
    this.pointerup = this.pointerup.bind(this);
    this.pointerleave = this.pointerleave.bind(this);
    this.animate = this.animate.bind(this);
    this.resize = this.resize.bind(this);
    this.lock = this.lock.bind(this);
    this.unlock = this.unlock.bind(this);
    this.goto = this.goto.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrev = this.gotoPrev.bind(this);
    this.move = this.move.bind(this);

    this.el = document.createElement('div');
    this.el.className = 'carousel';
    // user provided element
    if (el) {
      this.el = el;
      this.slideContainer = document.createElement('div');
      this.slideContainer.classList.add('dscarousel-slides');
      this.slideElements = Array.from(this.el.children); // .map(slide => ({ element: slide, offset: 0 }));
      this.slides = [];
      this.slideElements.forEach((slide) => {
        this.slideContainer.appendChild(slide);
        slide.classList.add('dscarousel-slides-slide');
        this.slides.push({ element: slide, offset: 0 });
      });
    }
    // user didn't supply element, create an empty carousel
    // user can then add slides using the .addSlide method
    else {
      this.slideContainer = document.createElement('div');
      this.slideContainer.classList.add('dscarousel-slides');
      this.slideContainer.parentNode.insertBefore(this.el, this.slideContainer);
    }
    this.el.classList.add('dscarousel');


    const { width, height } = this.slideContainer.getBoundingClientRect();
    // this.el.style.width = `${width}px`;
    // this.el.style.height = `${height}px`;

    this.state = {
      width: width || window.innerWidth, // TODO: if getBoundingClientRect().width is zero, add a mutation listener
      height: height || window.innerHeight,
      offsetLeft: 0,
      offsetTop: 0,
      current: 0,
      previous: 0,
      position: 0,
      speed: 0,
      dragging: false,
      wheeling: false,
      wasDragging: false,
      mobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
      percent: 0,
      locked: false,
      firstResize: true,
    };

    const defaults = {
      direction: 'horizontal',
      directionOverride: false, // both horizontal and vertical scrolling influence speed
      indicators: true,
      arrows: true,
      wrap: true, // true = infinite scrolling, false = clamp at either end
      inertia: 0.95, // 0-1, how much the carousel drifts on pointerup, 0 = none
      snapping: true, // end of drift should center current item
      keyboardEnabled: false, // respond to key events
      autoResize: true,
      full: false,
      debug: false,
    };

    this.options = Object.assign(defaults, options);

    this.createControls();
    this.listen();


    if (!el) {
      return this.el;
    }

    this.setDots();
    this.animate();
  }

  listen() {
    this.el.addEventListener('pointerenter', this.pointerenter);
    if (this.state.mobile) {
      this.el.addEventListener('touchstart', this.touchstart);
      this.el.addEventListener('touchmove', this.touchmove);
    } else {
      this.el.addEventListener('pointerdown', this.pointerdown);
      window.addEventListener('pointermove', this.pointermove);
    }
    window.addEventListener('pointerup', this.pointerup);
    this.el.addEventListener('pointerout', this.pointerleave);
    this.el.addEventListener('wheel', this.wheel);

    const resizeObserver = new ResizeObserver(this.resize);
    if (this.options.autoResize) resizeObserver.observe(this.slideContainer);

    const addedObserver = new MutationObserver(() => {
      if (document.contains(this.slideContainer)) {
        this.el.style.width = `${this.slideContainer.getBoundingClientRect().width}px`;
        this.el.style.height = `${this.slideContainer.getBoundingClientRect().height}px`;
        addedObserver.disconnect();
        if (!this.options.autoResize) this.resize();
      }
    });
    addedObserver.observe(document, { attributes: false, childList: true, characterData: false, subtree: true });
  }

  createControls() {
    this.controls = document.createElement('div');
    this.controls.className = 'dscarousel-controls';

    this.controlDots = document.createElement('div');
    this.controlDots.className = `dscarousel-controls-dots ${this.options.indicators ? '' : 'hidden'}`;

    this.controlArrows = document.createElement('div');
    this.controlArrows.className = `dscarousel-controls-arrows ${this.options.arrows ? '' : 'hidden'}`;

    this.controlArrowsLeft = document.createElement('span');
    this.controlArrowsLeft.className = 'dscarousel-controls-arrows-left';
    this.controlArrowsLeft.addEventListener('click', this.gotoPrev);
    this.controlArrowsRight = document.createElement('span');
    this.controlArrowsRight.className = 'dscarousel-controls-arrows-right';
    this.controlArrowsRight.addEventListener('click', this.gotoNext);
    this.controlArrows.appendChild(this.controlArrowsLeft);
    this.controlArrows.appendChild(this.controlArrowsRight);

    this.controls.appendChild(this.controlDots);
    this.controls.appendChild(this.controlArrows);
    this.el.appendChild(this.slideContainer);
    this.el.appendChild(this.controls);
  }

  addSlide(slide) {
    // TODO: does the new slide get pushed to this.slides?
    this.el.appendChild(slide);
    this.slideElements.push({ element: slide, offset: 0 });
    this.setDots();
  }

  // TODO
  removeSlide(slide) {
    this.setDots();
  }

  setDots() {
    this.controlDots.innerHTML = '';
    for (let i = 0; i < this.slides.length; i += 1) {
      const dot = document.createElement('span');
      dot.className = `dscarousel-controls-dots-dot ${i === this.state.current ? 'active' : ''}`;
      this.slides[i].dot = dot;
      this.controlDots.appendChild(dot);
    }
  }

  pointerdown() {
    if (this.state.locked) return;
    this.state.dragging = true;
  }

  touchstart(e) {
    if (this.state.locked) return;
    this.state.dragging = true;
    this.clientXPrev = e.changedTouches[0].clientX;
    this.clientYPrev = e.changedTouches[0].clientY;
  }

  pointerenter() {
    if (this.state.locked) return;
    this.state.dragging = this.state.wasDragging;
  }

  pointermove(e) {
    if (this.state.locked) return;
    if (this.state.dragging) {
      this.state.speed = this.options.direction === 'horizontal' ? e.movementX : e.movementY;
      // TODO: DRY
      this.state.position += this.state.speed;
      this.slideContainer.style.transform = `translateX(${this.state.position}px)`;
    }
  }

  touchmove(e) {
    if (this.state.locked) return;
    const { clientX, clientY } = e.changedTouches[0];
    this.state.speed = Math.abs(clientY - this.clientYPrev) > Math.abs(clientX - this.clientXPrev) ? clientY - this.clientYPrev : clientX - this.clientXPrev;
    this.clientXPrev = clientX;
    this.clientYPrev = clientY;
    this.state.position += this.state.speed;
    this.slideContainer.style.transform = `translateX(${this.state.position}px)`;
  }

  wheel(e) {
    if (this.state.locked) return;
    e.preventDefault();
    if (this.state.locked) return;
    this.state.wheeling = true;
    this.state.speed = Math.abs(e.deltaY) > Math.abs(e.deltaX) ? -e.deltaY / 3 : -e.deltaX;
    if (this.wheelingEnd) clearTimeout(this.wheelingEnd);
    this.wheelingEnd = setTimeout(this.pointerup, 1500);
    this.state.position += this.state.speed;
    this.slideContainer.style.transform = `translateX(${this.state.position}px)`;
  }

  pointerup() {
    if (this.state.locked) return;
    this.state.dragging = false;
    this.state.wasDragging = false;

    let velocityTemp = this.state.speed;
    let finalPosition = this.state.position;

    for (let i = 0; i < 200; i += 1) { // TODO: replace 200 with value related to options.inertia
      velocityTemp *= this.options.inertia;
      finalPosition += velocityTemp;
    }

    if (this.options.snapping) {
      const snappedFinalPosition = Math.round(finalPosition / this.state.width) * this.state.width;
      const velocityAdjustment = (snappedFinalPosition - finalPosition) / 19; // TODO: replace 19 with value ralated to options.inertia
      this.state.speed += velocityAdjustment;
    }
  }

  pointerleave() {
    if (this.state.locked) return;
    this.state.wasDragging = this.state.dragging;
  }

  animate() {
    if (!this.state.dragging) {
      if (Math.abs(this.state.speed) >= 0.01) {
        this.state.speed *= this.options.inertia;
        this.state.position += this.state.speed;
      }
    }

    // TODO: something in here is putting the slides out of order :(
    this.slides.forEach((slide) => {
      if (this.state.speed < 0) {
        if (parseInt(slide.element.getBoundingClientRect().left) < -this.state.width) {
          slide.offset += (this.state.width * this.slideElements.length);
          slide.element.style.transform = `translateX(${slide.offset}px)`;
        }
      } else {
        if (parseInt(slide.element.getBoundingClientRect().left) > this.state.width + this.state.offsetLeft) {
          slide.offset -= (this.state.width * this.slideElements.length);
          slide.element.style.transform = `translateX(${slide.offset}px)`;
        }
      }
    });

    let percent = (-this.state.position % (this.state.width * this.slides.length)) / (this.state.width * this.slides.length);
    if (percent < 0) percent = 1 + percent;

    // TODO: on init, percent is -0 which causes other problems, try to fix or don't start animate until this.state.position is established

    this.state.current = Math.round(percent * this.slides.length);
    if (this.state.current === this.slides.length) this.state.current = 0;
    if (this.state.current !== this.state.previous) {
      this.el.dispatchEvent(new CustomEvent('changed', { detail: this.state.current }));
      this.slides[this.state.previous].dot.classList.remove('active');
      this.slides[this.state.current].dot.classList.add('active');
    }
    this.state.previous = this.state.current;
    this.state.percent = percent;
    this.slideContainer.style.transform = `translateX(${this.state.position}px)`;

    this.el.dispatchEvent(new CustomEvent('update', { detail: this.state.percent }));

    requestAnimationFrame(this.animate);
  }

  resize() {
    // TODO: look inot why this is bouncing:
    const { width, height, left, top } = this.options.full ? document.body.getBoundingClientRect() : this.el.getBoundingClientRect();
    this.state.width = width;
    this.state.height = height;
    this.state.offsetLeft = left;
    this.state.offsetTop = top;

    if (this.options.debug) console.log('DS-Carousel: resized to', this.state.width, this.state.height);

    // stop the carousel
    this.state.speed = 0;

    // realign slides
    this.slides.forEach((slide, index) => {
      slide.offset = (this.state.width * index);
      slide.element.style.transform = `translateX(${slide.offset}px)`;
    });

    this.state.position = -this.state.current * this.state.width;

    if (this.state.firstResize) {
      this.state.firstResize = false;
      this.el.dispatchEvent(new Event('ready'));
    }
  }

  lock() {
    this.state.locked = true;
  }

  unlock() {
    this.state.locked = false;
  }

  // TODO: accept "immediate" param which moves carousel into position without animating
  goto(index, force) {
    // console.log('goto', index);
    return new Promise((resolve) => {
      if (index === this.state.current) return resolve();
      if (force) {
        this.state.position = this.state.width * -index;
        this.slideContainer.style.transform = `translateX(${this.state.position}px)`;
      }
      const leftStraight = { direction: 1, amount: this.state.current - index };
      const leftWrap = { direction: 1, amount: this.state.current - index + this.slides.length };
      const rightStraight = { direction: -1, amount: index - this.state.current };
      const rightWrap = { direction: -1, amount: index + this.slides.length - this.state.current };
      const values = [leftStraight, rightStraight, leftWrap, rightWrap];
      values.sort((a, b) => (a.amount > b.amount ? 1 : -1));
      const shortestMove = values.filter(item => item.amount >= 0)[0];
      this.move(shortestMove.amount * shortestMove.direction);
      this.waitForStop = setInterval(() => {
        if (Math.abs(this.state.speed) <= 0.1) {
          clearInterval(this.waitForStop);
          return resolve();
        }
      }, 10);
    });
  }

  gotoNext() {
    let newIndex = this.state.current + 1;
    if (newIndex >= this.slides.length) newIndex = 0;
    this.goto(newIndex);
  }

  gotoPrev() {
    let newIndex = this.state.current - 1;
    if (newIndex < 0) newIndex = this.slides.length - 1;
    this.goto(newIndex);
  }

  move(amount) {
    console.log('move', amount);
    const divisor = 3;// 3.09;
    const multiplier = 1.95;
    this.state.speed = (((this.state.width / this.slides.length) / divisor) * multiplier) * amount; // TODO <- Figure out the reason for these numbers
    let velocityTemp = this.state.speed;
    let finalPosition = this.state.position;

    for (let i = 0; i < 200; i += 1) {
      velocityTemp *= this.options.inertia;
      finalPosition += velocityTemp;
    }
    const snappedFinalPosition = Math.round(finalPosition / this.state.width) * this.state.width;
    const velocityAdjustment = (snappedFinalPosition - finalPosition) / 19.5; // TODO: not sure why 19.5 the value that works??? Maybe it's actually 1.95 and the .95 matches our damping value?
    this.state.speed += velocityAdjustment;
  }
}

window.Carousel = Carousel;
