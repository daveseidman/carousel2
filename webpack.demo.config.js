const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: './demo/index.js',
  devtool: 'source-map',
  // devServer: {
  //   hot: true,
  //   host: '0.0.0.0',
  //   historyApiFallback: true,
  // },
  plugins: [
    new CleanWebpackPlugin(['demo']),
    // new HtmlWebpackPlugin({ template: 'src/demo.html' }),
    // new webpack.HotModuleReplacementPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ],
      },
    ],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'demo/dist'),
  },
};
