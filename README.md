# Carousel
Simple but slick carousel that works with mouse, touch and trackpad events.


## Usage  

Here is a basic example of how to create a new DS-Carousel by giving it a container with your slides from HTML.  

**index.html**  
```
<div id='#myslides'>
  <div class='slide'>
    <h1>Slide #1 Title</h1>
    <p>Slide #1 Content here. Lorem Ipsum</p>
  </div>
  <div class='slide'>
    <h1>Slide #2 Title</h1>
    <p>Slide #2 Content here. Lorem Ipsum</p>
  </div>
</div>
```

**index.js**

```
import Carousel from 'ds-carousel';

const slides = document.querySelector('#slides');
const carousel = new Carousel(slides);
```

In this example, we'll create an empty DS-Carousel and then fill it using a for loop. This method might be better suited for procedurally generated carousels:

**index.js**
```
import Carousel from 'ds-carousel';

const carousel = new Carousel();
for (let i = 0; i < 3; i += 1) {
  const slide = document.createElement('div');
  slide.innerHTML = `<h1>Slide ${i}</h1>`;
  carousel.addSlide(slide);
}

document.body.appendChild(carousel.el);
```

Note: since this carousel was generated from code it still needs to be added to the DOM after initialization.



## API
